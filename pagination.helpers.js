export default class PaginationHelpers {
  #pageLinksWrapper = null;
  #code = "";
  #size = 0;
  #page = 0;
  #step = 0;

  /**
   *
   * @param {HTMLElement} container - The pagination container
   * @param {{size:number, page:number, step:number}} options - initial options
   */
  constructor(container, options) {
    this.#size = options.size;
    this.#page = options.page;
    this.#step = options.step;

    this.#create(container);
    this.#formatPageLinksWrapper();
  }

  // create skeleton
  #create(container) {
    const template = `
        <button class="pagination_nav pagination_nav_prev" >&#9668;</button>
        <ul class="pagination_body"></ul>
        <button class="pagination_nav pagination_nav_next">&#9658;</button>
      `;

    container.innerHTML = template;
    this.#pageLinksWrapper = container.querySelector("ul");
    this.#bindNavigationButtons(container);
  }

  #bindNavigationButtons(container) {
    const nav = container.querySelectorAll("button");

    nav[0].addEventListener("click", (e) => this.#prev(e));
    nav[1].addEventListener("click", (e) => this.#next(e));
  }

  // find pagination type
  #formatPageLinksWrapper() {
    if (this.#size < this.#step * 2 + 6) {
      this.#addPageLinks(1, this.#size + 1);
    } else if (this.#page < this.#step * 2 + 1) {
      this.#addPageLinks(1, this.#step * 2 + 4);
      this.#addListPageLinkShortcut();
    } else if (this.#page > this.#size - this.#step * 2) {
      this.#addFirstPageLinkShortcut();
      this.#addPageLinks(this.#size - this.#step * 2 - 2, this.#size + 1);
    } else {
      this.#addFirstPageLinkShortcut();
      this.#addPageLinks(this.#page - this.#step, this.#page + this.#step + 1);
      this.#addListPageLinkShortcut();
    }

    this.#drawPageLinks();
  }

  // add page by number
  #addPageLinks(start, end) {
    for (let i = start; i < end; i++) {
      this.#code += `<li>${i}</li>`;
    }
  }

  #addFirstPageLinkShortcut() {
    this.#code += `<li>1</li><i>...</i>`;
  }

  // add last page with separator
  #addListPageLinkShortcut() {
    this.#code += `<i>...</i><li>${this.#size}</li>`;
  }

  // previous page
  #prev() {
    this.#page--;
    if (this.#page < 1) {
      this.#page = 1;
    }

    this.#formatPageLinksWrapper();
  }

  // next page
  #next() {
    this.#page++;
    if (this.#page > this.#size) {
      this.#page = this.#size;
    }

    this.#formatPageLinksWrapper();
  }

  // write pagination
  #drawPageLinks() {
    this.#pageLinksWrapper.innerHTML = this.#code;
    this.#code = "";
    this.#bind();
  }

  #bind() {
    const links = this.#pageLinksWrapper.querySelectorAll("li");

    for (let link of links) {
      if (+link.innerHTML === this.#page) {
        link.classList.add("current");
      }
      link.addEventListener("click", (e) => this.#click(e));
    }
  }

  // change page
  #click(event) {
    this.#page = +event.target.innerHTML;
    this.#formatPageLinksWrapper();
  }
}

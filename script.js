import PaginationHelpers from "./pagination.helpers.js";

function main() {
  try {
    const container = document.querySelector("#pagination");
    new PaginationHelpers(container, {
      size: 30, // pages size
      step: 3, // pages before and after current
      page: 1, // selected page
    });
  } catch (error) {
    console.log(error);
  }
}

main();
